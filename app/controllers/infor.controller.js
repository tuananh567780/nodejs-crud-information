const db = require("../models");
const Information = db.infor;
const Op = db.Sequelize.Op;


// create and save a new information
exports.create = (req, res) => {
    if (!req.body.name) {
        res.status(400).send({
            message: " Không được để trông nội dung."
        });
        return;
    }

    // create an information
    const infor = {
        name: req.body.name,
        address: req.body.address,
        email: req.body.email,
        numphone: req.body.numphone,
        published: req.body.published ? req.body.published : false
    };
    // save the information in the database
    Information.create(infor) 
    .then(data => {
        // send the information to the client
        res.status(201).send(data);
    })
    .catch(err => {
        res.status(400).send({
            message:
            err.message || "Đã xảy ra một số lỗi khi tạo thông tin."
        });
    });
};

// Tìm tất cả thông tin trong cơ sở dữ liệu
// Retrieve all information from the database.
exports.findAll = (req, res) => {
    const name = req.body.name;
    var condition = name ? {name: {[Op.like]: `%${name}`}} : null;

    Information.findAll({where: condition})
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message:
            err.message || "Đã xảy ra một số lỗi khi truy xuất thông tin."
        });
    });
};

// find one with id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Information.findByPk(id)
    .then(data => {
        if (data) {
            res.send(data);
        } else {
            res.status(404).send({
                message: "Không tìm thấy thông tin với id= ${id}."
            });
        }
    })
    .catch(err => {
        res.status(500).send({
            message:
            err.message || "Đã xảy ra môt số mỗi khi tìm với id= " +id
        });
    });
};

// Update a information by the id in the request
//Cập nhật thoong tin theo id trong yêu cầu
exports.update = (req, res) => {
    const id = req.params.id;
    
    Information.update( req.body,{
        where: {id:id}
    } )
    .then(num => {
        if (num == 1) {
            res.send({
                message: "Thong tin đã được cập nhật thành công."
            });
        }
        else {
            res.status(400).send({
                message: "Không thể cập nhật Thong tin với id=${id}. Có thể Thong tin không được tìm thấy hoặc req.body trống"
            });
        }
    })
    .catch(err => {
        res.status(500).send({
            message:
            err.message || "Lỗi khi cập nhật thong tin với id=" +id
        });
    });
};

//Delete a information with the specified id in the request
//Xóa thông tin theo  trong yêu cầu
exports.delete = (req, res) => {
    const id = req.params.id;
    Information.destroy({
        where: {id:id}
    })
    .then(num =>{
        if(num == 1) {
            res.status(200).send({
                message: "Thông tin đã được xóa thành công."
            });
        } else {
            res.status(400).send({
                message: "Không thể xoa Thong tin với id=${id}. Có thể Thong tin không được tìm thấy hoặc req.body trống"
            });
        }
    })
    .catch(err => {
        res.status(500).send({
            message:
            err.message || "Lỗi khi xoa thong tin với id=" +id
        });
    });
};

// delete all
exports.deleteAll = (req, res) => {
    Information.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} Thông tin đã được xoá thành công!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Đã xảy ra lỗi khi xóa tất cả thông tin."
        });
      });
  };

// Tìm tất cả thông tin đã xuất bản
// find all published information
exports.findAllPublished = (req, res) => {
    Information.findAll({ where: { published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Đã xảy ra một số lỗi khi truy xuất thong tin."
      });
    });
}