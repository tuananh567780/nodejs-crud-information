

module.exports = app => {

    const infor = require("../controllers/infor.controller.js");
    var router = require("express").Router();

    // Create a new information
    router.post("/informations", infor.create);

    
    // Retrieve all information from the database.
    router.get("/informations", infor.findAll);

    //Retrieve all published information
    router.get("/informations/published", infor.findAllPublished);

    // Retrieve information by id
    router.get("/informations/:id", infor.findOne);

    // Update new an information
    router.put("/informations/:id", infor.update);

    // Delete one  information
    router.delete("/informations/:id", infor.delete);

    //delete all information
    router.delete("/informations", infor.deleteAll);

    app.use("/api", router);
};