// noinspection JSValidateTypes

const express = require("express");

const cors = require("cors");

const app = express();

const corsOptions = {
  origin: "http://localhost:8091"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());  

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

const db = require("./app/models");
const {application} = require("express");
db.sequelize.sync();


// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to @Tuananh00069  Information application." });
});

require("./app/routes/infor.routes")(app);
/*
module.exports = app => {
  // code here
  require("./app/routes/infor.routes")(app);
};
*/

// set port, listen for requests
const PORT = process.env.PORT || 8090;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
